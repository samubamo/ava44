package utfpr.ct.dainf.pratica;

import java.util.Comparator;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator extends Lancamento implements Comparator<Lancamento> {
    
    @Override
    public int compare(Lancamento l1, Lancamento l2){
        if(l1.getConta() == l2.getConta()){
            if(l1.getData().compareTo(l1.getData()) == -1){
                return -1;
            }
            if(l1.getData().compareTo(l1.getData()) == 1){
                return 1;
            }
            else{
                return 1;
            }
        }
        return 0;
    }
    
    
}
